class CommentsController < ApplicationController
  
  before_action :authenticate_user!
  
  #コメントの投稿
  def create
    @comment = Comment.new(comment_params)
    @post = @comment.post
    if @comment.save
      # ここから
      @post.create_notification_comment!(current_user, @comment.id)
      # ここまで
      respond_to :js
    else
      flash[:alert] = "コメントの投稿に失敗しました"
    end
  end
  
  #コメントの削除
  def destroy
    @comment = Comment.find_by(id: params[:id])
    @post = @comment.post
    if @comment.destroy
      respond_to :js
    else
      flash[:alert] = "コメントの削除に失敗しました"
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:user_id, :post_id, :comment)
    end
end

