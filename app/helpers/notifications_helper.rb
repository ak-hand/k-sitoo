module NotificationsHelper
  def unchecked_notifications
    @notifications=current_user.passive_notifications.where(checked: false)
  end
  def notification_form(notification)
  @comment=nil
  visitor=link_to notification.visitor.name, notification.visitor, style:"font-weight: bold;"
  your_post=link_to 'あなたの投稿', notification.post, style:"font-weight: bold;", remote: true
  case notification.action
    when "like" then
      "#{visitor}が#{your_post}にいいね！しました"
    when "comment" then
      "#{visitor}が#{your_post}にコメントしました"
  end
end
end
