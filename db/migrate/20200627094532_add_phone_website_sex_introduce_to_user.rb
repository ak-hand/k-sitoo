class AddPhoneWebsiteSexIntroduceToUser < ActiveRecord::Migration[5.1]
  def change
     add_column :users, :phone, :string, null: false, default: ""
     add_column :users, :website, :string, null: false, default: ""
     add_column :users, :sex, :string, null: false, default: ""
     add_column :users, :introduction, :string, null: false, default: ""
        
  end
end
